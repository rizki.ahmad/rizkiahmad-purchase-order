USE [evaluation]
GO
/****** Object:  Table [dbo].[m_city]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_city](
	[city_id] [varchar](10) NOT NULL,
	[city_name] [varchar](50) NOT NULL,
	[prov_id] [varchar](10) NOT NULL,
 CONSTRAINT [PK_m_city] PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_item]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_item](
	[item_id] [varchar](10) NOT NULL,
	[item_name] [varchar](50) NOT NULL,
	[item_price] [int] NOT NULL,
	[sup_id] [varchar](10) NOT NULL,
 CONSTRAINT [PK_m_item] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_province]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_province](
	[prov_id] [varchar](10) NOT NULL,
	[prov_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_m_province] PRIMARY KEY CLUSTERED 
(
	[prov_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_supplier]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_supplier](
	[sup_id] [varchar](10) NOT NULL,
	[sup_name] [varchar](50) NULL,
	[sup_address] [varchar](100) NULL,
 CONSTRAINT [PK_m_supplier] PRIMARY KEY CLUSTERED 
(
	[sup_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_po]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_po](
	[po_no] [varchar](20) NOT NULL,
	[sup_id] [varchar](10) NOT NULL,
	[po_address] [varchar](100) NOT NULL,
	[city_id] [varchar](10) NOT NULL,
	[po_date] [datetime] NULL,
	[po_exp_date] [datetime] NULL,
	[po_shipment] [char](1) NOT NULL,
	[po_notes] [varchar](50) NULL,
	[discount] [float] NOT NULL,
	[total] [float] NOT NULL,
 CONSTRAINT [PK_t_po] PRIMARY KEY CLUSTERED 
(
	[po_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_po_detail]    Script Date: 14/02/2022 17.42.33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_po_detail](
	[po_no] [varchar](20) NOT NULL,
	[item_id] [varchar](10) NOT NULL,
	[item_price] [int] NOT NULL,
	[item_qty] [int] NOT NULL,
	[subtotal] [float] NOT NULL,
 CONSTRAINT [PK_t_po_detail] PRIMARY KEY CLUSTERED 
(
	[po_no] ASC,
	[item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[m_city] ([city_id], [city_name], [prov_id]) VALUES (N'1', N'Jakarta Barat', N'1')
INSERT [dbo].[m_city] ([city_id], [city_name], [prov_id]) VALUES (N'2', N'Jakarta Timur', N'1')
INSERT [dbo].[m_city] ([city_id], [city_name], [prov_id]) VALUES (N'4', N'Jakarta Utara', N'1')
INSERT [dbo].[m_city] ([city_id], [city_name], [prov_id]) VALUES (N'7', N'Bandung', N'2')
INSERT [dbo].[m_item] ([item_id], [item_name], [item_price], [sup_id]) VALUES (N'1', N'Meja Kaca', 4000000, N'1')
INSERT [dbo].[m_item] ([item_id], [item_name], [item_price], [sup_id]) VALUES (N'2', N'Bangku Kayu', 100000, N'2')
INSERT [dbo].[m_item] ([item_id], [item_name], [item_price], [sup_id]) VALUES (N'3', N'Bangku Plastik', 150000, N'2')
INSERT [dbo].[m_province] ([prov_id], [prov_name]) VALUES (N'1', N'DKI Jakarta')
INSERT [dbo].[m_province] ([prov_id], [prov_name]) VALUES (N'2', N'Jawa Barat')
INSERT [dbo].[m_province] ([prov_id], [prov_name]) VALUES (N'3', N'Sumatera Barat')
INSERT [dbo].[m_supplier] ([sup_id], [sup_name], [sup_address]) VALUES (N'1', N'PT.Sukses Jaya', N'Jl. Sukses Jaya No.1')
INSERT [dbo].[m_supplier] ([sup_id], [sup_name], [sup_address]) VALUES (N'2', N'PT.Terus Maju', N'Jl. Terus Maju No.1')
INSERT [dbo].[t_po] ([po_no], [sup_id], [po_address], [city_id], [po_date], [po_exp_date], [po_shipment], [po_notes], [discount], [total]) VALUES (N'P01900', N'1', N'Jl. Sukses Jaya No.1', N'4', CAST(N'2022-02-14T00:00:00.000' AS DateTime), CAST(N'2022-02-23T00:00:00.000' AS DateTime), N'P', N'TESTING', 10, 7200000)
INSERT [dbo].[t_po] ([po_no], [sup_id], [po_address], [city_id], [po_date], [po_exp_date], [po_shipment], [po_notes], [discount], [total]) VALUES (N'PO101140222', N'1', N'DAAN MOGOT, JAKARTA BARAT,', N'1', CAST(N'2022-02-14T00:00:00.000' AS DateTime), CAST(N'2022-02-28T00:00:00.000' AS DateTime), N'P', N'PURCHASE BARANG', 0, 20000000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'123', N'1', 4000000, 2, 8000000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'12310', N'2', 100000, 1, 100000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'12310', N'3', 150000, 1, 150000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'33', N'2', 100000, 6, 100000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'P01900', N'1', 4000000, 2, 8000000)
INSERT [dbo].[t_po_detail] ([po_no], [item_id], [item_price], [item_qty], [subtotal]) VALUES (N'PO101140222', N'1', 4000000, 5, 20000000)
