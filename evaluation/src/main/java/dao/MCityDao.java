package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MCity;
import entity.MCityPK;

public interface MCityDao extends JpaRepository<MCity, MCityPK>{
	@Query("SELECT c FROM MCity AS c WHERE c.provId LIKE %:provId%")
	List<MCity>findAllCityByProvId(@Param("provId") String provId);
}
