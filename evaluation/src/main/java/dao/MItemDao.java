package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MItem;
import entity.MItemPK;

public interface MItemDao extends JpaRepository<MItem, MItemPK>{
	@Query("SELECT i FROM MItem AS i WHERE i.supId LIKE %:supId%")
	List<MItem>findAllItemBySupId(@Param("supId") String supId);
}
