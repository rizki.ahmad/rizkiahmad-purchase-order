package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.TPo;
import entity.TPoPK;

public interface TPoDao extends JpaRepository<TPo, TPoPK>{
	
	@Query("SELECT p, s.supName FROM TPo AS p, MSupplier AS s "
			+ "WHERE (p.supId = s.supId) AND "
			+ "(p.poNo LIKE %:search% OR s.supName LIKE %:search%)")
	List<Object[]>findAllPoBySearch(@Param("search")String search);
}
