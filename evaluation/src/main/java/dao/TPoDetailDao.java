package dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.TPoDetail;
import entity.TPoDetailPK;

public interface TPoDetailDao extends JpaRepository<TPoDetail, TPoDetailPK>{
	
	@Query("SELECT d FROM TPoDetail AS d WHERE d.poNo = :poNo")
	List<TPoDetail>findAllDetailByPoNo(@Param("poNo") String poNo);
	
	@Modifying
	@Query("DELETE FROM TPoDetail WHERE poNo = :poNo")
	void deleteDetailByPoNo(@Param("poNo") String poNo);
}
