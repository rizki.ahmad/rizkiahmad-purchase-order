package dto;

import java.util.Date;
import java.util.List;

public class TPoDto {
	private String poNo;
	private String cityId;
	private int discount;
	private String poAddress;
	private Date poDate;
	private Date poExpDate;
	private String poNotes;
	private String poShipment;
	private String supId;
	private int total;
	private String cityName;
	private String supName;
	private List<TPoDetailDto> listDetail;
	private boolean cheked;
	
	public List<TPoDetailDto> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<TPoDetailDto> listDetail) {
		this.listDetail = listDetail;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public String getPoAddress() {
		return poAddress;
	}
	public void setPoAddress(String poAddress) {
		this.poAddress = poAddress;
	}
	public Date getPoDate() {
		return poDate;
	}
	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
	public Date getPoExpDate() {
		return poExpDate;
	}
	public void setPoExpDate(Date poExpDate) {
		this.poExpDate = poExpDate;
	}
	public String getPoNotes() {
		return poNotes;
	}
	public void setPoNotes(String poNotes) {
		this.poNotes = poNotes;
	}
	public String getPoShipment() {
		return poShipment;
	}
	public void setPoShipment(String poShipment) {
		this.poShipment = poShipment;
	}
	public String getSupId() {
		return supId;
	}
	public void setSupId(String supId) {
		this.supId = supId;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getSupName() {
		return supName;
	}
	public void setSupName(String supName) {
		this.supName = supName;
	}
	public boolean isCheked() {
		return cheked;
	}
	public void setCheked(boolean cheked) {
		this.cheked = cheked;
	}

	
}
