package service;

import java.util.List;

import dto.MCityDto;

public interface MCitySvc {
	public List<MCityDto> findAllCityByProvId(String provId);
	public List<MCityDto> findAll();
	public MCityDto findOne(String cityId);
}
