package service;

import java.util.List;

import dto.MItemDto;

public interface MItemSvc {
	public List<MItemDto> findAllItemBySupId(String supId);
	public List<MItemDto> findAll();
	public MItemDto findOne(String itemId);
}
