package service;

import java.util.List;

import dto.MProvinceDto;

public interface MProvinceSvc {
	public List<MProvinceDto>findAll();
	public MProvinceDto findOne(String provId);
}
