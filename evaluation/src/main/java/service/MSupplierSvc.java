package service;

import java.util.List;

import dto.MSupplierDto;

public interface MSupplierSvc {
	public List<MSupplierDto> findAll();
	public MSupplierDto findOne(String supId);
}
