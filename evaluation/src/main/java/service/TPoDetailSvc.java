package service;

import java.util.List;

import dto.TPoDetailDto;

public interface TPoDetailSvc {
	public List<TPoDetailDto> findAllDetailByPoNo(String poNo);
	public void deleteDetailByPoNo (String poNo);
	public void save(TPoDetailDto dto);
	public void delete (String poNo, String itemId);
	public TPoDetailDto findOne (String poNo, String itemId);
}
