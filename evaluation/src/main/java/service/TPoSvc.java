package service;

import java.util.List;

import dto.TPoDto;

public interface TPoSvc {
	public List<TPoDto> findAllPoBySearch(String search);
	public TPoDto findOne(String poNo);
	public void save (TPoDto dto);
	public void delete (String poNo);
}
