package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MCityDao;
import dao.MProvinceDao;
import dto.MCityDto;
import entity.MCity;
import entity.MCityPK;
import entity.MProvince;
import entity.MProvincePK;
import service.MCitySvc;

@Service("citySvc")
@Transactional
public class MCitySvcImpl implements MCitySvc{

	@Autowired
	MCityDao cityDao;
	
	@Autowired
	MProvinceDao provinceDao;
	
	@Override
	public List<MCityDto> findAllCityByProvId(String provId) {
		// TODO Auto-generated method stub
		List<MCityDto> dtos = new ArrayList<>();
		List<MCity> list = cityDao.findAllCityByProvId(provId);
		for (MCity c : list) {
			MCityDto dto = new MCityDto();
			dto.setCityId(c.getCityId());
			dto.setCityName(c.getCityName());
			dto.setProvId(c.getProvId());
			dto.setProvName(getProvName(c.getProvId()));
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<MCityDto> findAll() {
		// TODO Auto-generated method stub
		List<MCityDto> dtos = new ArrayList<>();
		List<MCity> list = cityDao.findAll();
		for (MCity c : list) {
			MCityDto dto = new MCityDto();
			dto.setCityId(c.getCityId());
			dto.setCityName(c.getCityName());
			dto.setProvId(c.getProvId());
			dto.setProvName(getProvName(c.getProvId()));
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public MCityDto findOne(String cityId) {
		// TODO Auto-generated method stub
		MCityPK id = new MCityPK();
		id.setCityId(cityId);
		MCity c = cityDao.findOne(id);
		if (c != null) {
			MCityDto dto = new MCityDto();
			dto.setCityId(c.getCityId());
			dto.setCityName(c.getCityName());
			dto.setProvId(c.getProvId());
			dto.setProvName(getProvName(c.getProvId()));
			return dto;
		}
		return null;
	}

	public String getProvName(String provId){
		MProvincePK id = new MProvincePK();
		id.setProvId(provId);
		MProvince p = provinceDao.findOne(id);
		if (p != null) {
			return p.getProvName();
		} else {
			return "Prov name not found";
		}
	}
}
