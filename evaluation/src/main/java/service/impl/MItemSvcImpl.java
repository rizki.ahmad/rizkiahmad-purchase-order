package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MItemDao;
import dao.MSupplierDao;
import dto.MItemDto;
import entity.MItem;
import entity.MItemPK;
import entity.MSupplier;
import entity.MSupplierPK;
import service.MItemSvc;

@Service("itemSvc")
@Transactional
public class MItemSvcImpl implements MItemSvc{

	@Autowired
	MItemDao itemDao;
	
	@Autowired
	MSupplierDao supplierDao;
	
	@Override
	public List<MItemDto> findAllItemBySupId(String supId) {
		// TODO Auto-generated method stub
		List<MItemDto> dtos = new ArrayList<>();
		List<MItem> list = itemDao.findAllItemBySupId(supId);
		for (MItem i : list) {
			MItemDto dto = new MItemDto();
			dto.setItemId(i.getItemId());
			dto.setItemName(i.getItemName());
			dto.setItemPrice(i.getItemPrice());
			dto.setSupId(i.getSupId());
			dto.setSupName(getSuppName(i.getSupId()));
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public List<MItemDto> findAll() {
		// TODO Auto-generated method stub
		List<MItemDto> dtos = new ArrayList<>();
		List<MItem> list = itemDao.findAll();
		for (MItem i : list) {
			MItemDto dto = new MItemDto();
			dto.setItemId(i.getItemId());
			dto.setItemName(i.getItemName());
			dto.setItemPrice(i.getItemPrice());
			dto.setSupId(i.getSupId());
			dto.setSupName(getSuppName(i.getSupId()));
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public MItemDto findOne(String itemId) {
		// TODO Auto-generated method stub
		MItemPK id = new MItemPK();
		id.setItemId(itemId);
		MItem i = itemDao.findOne(id);
		if (i != null) {
			MItemDto dto = new MItemDto();
			dto.setItemId(i.getItemId());
			dto.setItemName(i.getItemName());
			dto.setItemPrice(i.getItemPrice());
			dto.setSupId(i.getSupId());
			dto.setSupName(getSuppName(i.getSupId()));
			return dto;
		}
		return null;
	}

	public String getSuppName(String supId){
		MSupplierPK id = new MSupplierPK();
		id.setSupId(supId);
		MSupplier s = supplierDao.findOne(id);
		if (s != null) {
			return s.getSupName();
		} else {
			return "Supp name not found";
		}
	}
}
