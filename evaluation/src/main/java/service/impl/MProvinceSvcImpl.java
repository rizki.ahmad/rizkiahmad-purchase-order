package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MProvinceDao;
import dto.MProvinceDto;
import entity.MProvince;
import entity.MProvincePK;
import service.MProvinceSvc;

@Service("provinceSvc")
@Transactional
public class MProvinceSvcImpl implements MProvinceSvc{

	@Autowired
	MProvinceDao provinceDao;
	
	@Override
	public List<MProvinceDto> findAll() {
		// TODO Auto-generated method stub
		List<MProvinceDto> dtos = new ArrayList<>();
		List<MProvince> list = provinceDao.findAll();
		for (MProvince p : list) {
			MProvinceDto dto = new MProvinceDto();
			dto.setProvId(p.getProvId());
			dto.setProvName(p.getProvName());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public MProvinceDto findOne(String provId) {
		// TODO Auto-generated method stub
		MProvincePK id = new MProvincePK();
		id.setProvId(provId);
		MProvince p = provinceDao.findOne(id);
		if (p != null) {
			MProvinceDto dto = new MProvinceDto();
			dto.setProvId(p.getProvId());
			dto.setProvName(p.getProvName());
			return dto;
		}
		return null;
	}

}
