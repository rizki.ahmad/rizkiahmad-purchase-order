package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MSupplierDao;
import dto.MSupplierDto;
import entity.MSupplier;
import entity.MSupplierPK;
import service.MSupplierSvc;

@Service("supplierSvc")
@Transactional
public class MSupplierSvcImpl implements MSupplierSvc{

	@Autowired
	MSupplierDao supplierDao;
	
	@Override
	public List<MSupplierDto> findAll() {
		// TODO Auto-generated method stub
		List<MSupplierDto> dtos = new ArrayList<>();
		List<MSupplier> list = supplierDao.findAll();
		for (MSupplier s : list) {
			MSupplierDto dto = new MSupplierDto();
			dto.setSupAddress(s.getSupAddress());
			dto.setSupId(s.getSupId());
			dto.setSupName(s.getSupName());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public MSupplierDto findOne(String supId) {
		// TODO Auto-generated method stub
		MSupplierPK id = new MSupplierPK();
		id.setSupId(supId);
		MSupplier s = supplierDao.findOne(id);
		if (s != null) {
			MSupplierDto dto = new MSupplierDto();
			dto.setSupAddress(s.getSupAddress());
			dto.setSupId(s.getSupId());
			dto.setSupName(s.getSupName());
			return dto;
		}
		return null;
	}

}
