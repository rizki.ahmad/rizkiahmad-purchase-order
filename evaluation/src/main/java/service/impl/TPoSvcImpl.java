package service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.MCityDao;
import dao.MItemDao;
import dao.MProvinceDao;
import dao.MSupplierDao;
import dao.TPoDao;
import dao.TPoDetailDao;
import dto.TPoDetailDto;
import dto.TPoDto;
import entity.MCity;
import entity.MCityPK;
import entity.MItem;
import entity.MItemPK;
import entity.MProvince;
import entity.MProvincePK;
import entity.MSupplier;
import entity.MSupplierPK;
import entity.TPo;
import entity.TPoDetail;
import entity.TPoPK;
import service.TPoSvc;

@Service("poSvc")
@Transactional
public class TPoSvcImpl implements TPoSvc{

	@Autowired
	TPoDao poDao;
	
	@Autowired
	TPoDetailDao detailDao;
	
	@Autowired
	MSupplierDao supplierDao;
	
	@Autowired
	MItemDao itemDao;
	
	@Autowired
	MCityDao cityDao;
	
	@Autowired
	MProvinceDao provinceDao;
	
	@Override
	public List<TPoDto> findAllPoBySearch(String search) {
		// TODO Auto-generated method stub
		List<TPoDto> dtos = new ArrayList<>();
		List<Object[]> list = poDao.findAllPoBySearch(search);
		for (Object[] o : list) {
			TPoDto dto = new TPoDto();
			TPo p = (TPo)o[0];
			dto.setCityId(p.getCityId());
			dto.setCityName(getCityName(p.getCityId()));
			dto.setDiscount(p.getDiscount());
			dto.setListDetail(getListDetail(p.getPoNo()));
			dto.setPoAddress(p.getPoAddress());
			dto.setPoDate(p.getPoDate());
			dto.setPoExpDate(p.getPoExpDate());
			dto.setPoNo(p.getPoNo());
			dto.setPoNotes(p.getPoNotes());
			dto.setPoShipment(p.getPoShipment());
			dto.setSupId(p.getSupId());
			dto.setSupName((String)o[1]);
			dto.setTotal(p.getTotal());
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public TPoDto findOne(String poNo) {
		// TODO Auto-generated method stub
		TPoPK id = new TPoPK();
		id.setPoNo(poNo);
		TPo p = poDao.findOne(id);
		if (p != null) {
			TPoDto dto = new TPoDto();
			dto.setCityId(p.getCityId());
			dto.setCityName(getCityName(p.getCityId()));
			dto.setDiscount(p.getDiscount());
			dto.setListDetail(getListDetail(p.getPoNo()));
			dto.setPoAddress(p.getPoAddress());
			dto.setPoDate(p.getPoDate());
			dto.setPoExpDate(p.getPoExpDate());
			dto.setPoNo(p.getPoNo());
			dto.setPoNotes(p.getPoNotes());
			dto.setPoShipment(p.getPoShipment());
			dto.setSupId(p.getSupId());
			dto.setSupName(getSuppName(p.getSupId()));
			dto.setTotal(p.getTotal());
			return dto;
		}
		return null;
	}

	@Override
	public void save(TPoDto dto) {
		// TODO Auto-generated method stub
		TPo p = new TPo();
		p.setCityId(dto.getCityId());
		p.setDiscount(dto.getDiscount());
		p.setPoAddress(dto.getPoAddress());
		p.setPoDate(dto.getPoDate());
		p.setPoExpDate(dto.getPoExpDate());
		p.setPoNo(dto.getPoNo());
		p.setPoNotes(dto.getPoNotes());
		p.setPoShipment(dto.getPoShipment());
		p.setSupId(dto.getSupId());
		p.setTotal(dto.getTotal());
		poDao.save(p);
		detailDao.deleteDetailByPoNo(dto.getPoNo());
		List<TPoDetailDto> listDetail = dto.getListDetail();
		for (TPoDetailDto d : listDetail) {
			TPoDetail det = new TPoDetail();
			det.setItemId(d.getItemId());
			det.setItemPrice(d.getItemPrice());
			det.setItemQty(d.getItemQty());
			det.setPoNo(d.getPoNo());
			det.setSubtotal(d.getSubtotal());
			detailDao.save(det);
		}
	}

	@Override
	public void delete(String poNo) {
		// TODO Auto-generated method stub
		TPoPK id = new TPoPK();
		id.setPoNo(poNo);
		poDao.delete(id);
		detailDao.deleteDetailByPoNo(poNo);
	}
	
	public String getCityName(String cityId){
		MCityPK id = new MCityPK();
		id.setCityId(cityId);
		MCity c = cityDao.findOne(id);
		if (c != null) {
			return c.getCityName();
		} else {
			return "City name not found";
		}
	}
	
	public String getProvName(String provId){
		MProvincePK id = new MProvincePK();
		id.setProvId(provId);
		MProvince p = provinceDao.findOne(id);
		if (p != null) {
			return p.getProvName();
		} else {
			return "Prov name not found";
		}
	}
	
	public String getSuppName(String supId){
		MSupplierPK id = new MSupplierPK();
		id.setSupId(supId);
		MSupplier s = supplierDao.findOne(id);
		if (s != null) {
			return s.getSupName();
		} else {
			return "Supp name not found";
		}
	}
	
	public List<TPoDetailDto> getListDetail(String poNo){
		List<TPoDetailDto> dtos = new ArrayList<>();
		List<TPoDetail> list = detailDao.findAllDetailByPoNo(poNo);
		for (TPoDetail d : list) {
			TPoDetailDto dto = new TPoDetailDto();
			dto.setItemId(d.getItemId());
			dto.setItemName(getItemName(d.getItemId()));
			dto.setItemPrice(d.getItemPrice());
			dto.setItemQty(d.getItemQty());
			dto.setPoNo(d.getPoNo());
			dto.setSubtotal(d.getSubtotal());
			dtos.add(dto);
		}
		return dtos;
	}
	
	public String getItemName(String itemId){
		MItemPK id = new MItemPK();
		id.setItemId(itemId);
		MItem i = itemDao.findOne(id);
		if (i != null) {
			return i.getItemName();
		} else {
			return "item name not found";
		}			
	}
}
