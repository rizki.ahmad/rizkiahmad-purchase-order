package vmd;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Messagebox;

import dto.MCityDto;
import dto.MItemDto;
import dto.MProvinceDto;
import dto.MSupplierDto;
import dto.TPoDetailDto;
import dto.TPoDto;
import service.MCitySvc;
import service.MItemSvc;
import service.MProvinceSvc;
import service.MSupplierSvc;
import service.TPoSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class TrxPoDetailVmd {

	@WireVariable
	TPoSvc poSvc;
	
	@WireVariable
	MSupplierSvc supplierSvc;
	
	@WireVariable
	MItemSvc itemSvc;
	
	@WireVariable
	MProvinceSvc provinceSvc;
	
	@WireVariable
	MCitySvc citySvc;
	
	private TPoDto dtoPo = new TPoDto();
	private List<TPoDetailDto> listDetail = new ArrayList<>();
	private List<TPoDetailDto> selectedListDetail = new ArrayList<>();
	private TPoDetailDto dtoDetail = new TPoDetailDto();
	private List<MSupplierDto> listSupp = new ArrayList<>();
	private MSupplierDto dtoSupp = new MSupplierDto();
	private List<MItemDto> listItem = new  ArrayList<>();
	private MItemDto dtoItem = new MItemDto();
	private List<MProvinceDto> listProv = new ArrayList<>();
	private MProvinceDto dtoProv = new MProvinceDto();
	private List<MCityDto> listCity = new ArrayList<>();
	private MCityDto dtoCity = new MCityDto();
	private long aging = 0;
	private boolean flagEdit = false;
	private boolean buttonVisible = true;
	private boolean modal = false;
	private boolean lock = false;
	private String condition="";
	
	@Init
	public void loadData() {
		listSupp = supplierSvc.findAll();
		listProv = provinceSvc.findAll();
		if (Sessions.getCurrent().getAttribute("obj")!=null) {
			dtoPo = (TPoDto)Sessions.getCurrent().getAttribute("obj");
			listDetail = dtoPo.getListDetail();
			sumTotal();
			dtoCity = citySvc.findOne(dtoPo.getCityId());
			dtoProv = provinceSvc.findOne(dtoCity.getProvId());
			dtoSupp = supplierSvc.findOne(dtoPo.getSupId());
			countAging();
			flagEdit = true;
			buttonVisible = true;
			lock = true;
			condition="update";
		} else {
			dtoPo = new TPoDto();
			listDetail = new ArrayList<>();
			flagEdit = false;
			lock = false;
			condition = "save";
		}
	}
	
	@Command
	public void cancel(){
		Executions.sendRedirect("TrxPo.zul");
	}
	
	@Listen("onClick")
	@Command
	public void save() {
		TPoDto findOne = poSvc.findOne(dtoPo.getPoNo());
		if (!checkPo()) {
			Messagebox.show("Please complete purchase order data");
		} else if (findOne != null && !flagEdit) {
			Messagebox.show("Order ID Already Exist");
		} else {
			countAging();
			if (listDetail.size()<1) {
				Messagebox.show("Please add product atleast 1");
			} else if (aging<1) {
				Messagebox.show("Aging cant below 1");
			} else if (dtoPo.getPoShipment()== null) {
				Messagebox.show("Please select shipment");
			} else if (dtoPo.getDiscount()>100 || dtoPo.getDiscount()< 0) {
				Messagebox.show("Discount only between 0 - 100%");
			} else {
				dtoPo.setListDetail(listDetail);
				dtoPo.setSupId(dtoSupp.getSupId());
				dtoPo.setCityId(dtoCity.getCityId());
				sumTotal();
				Messagebox.show("Are you sure want to "+condition+" Po Number : "+dtoPo.getPoNo()+",Total = "+dtoPo.getTotal()+"?", condition+" purchase order?",Messagebox.YES | Messagebox.NO,
						Messagebox.QUESTION, new EventListener<Event>() {
							@Override
							public void onEvent(Event event) throws Exception {
								// TODO Auto-generated method stub
								if (Messagebox.ON_YES.equals(event.getName())) {
									poSvc.save(dtoPo);
									Messagebox.show("Order saved successfully");
									Executions.sendRedirect("TrxPo.zul");
								} else {
									Messagebox.show(condition+" Po Number : "+dtoPo.getPoNo()+",Total = "+dtoPo.getTotal()+". Canceled");
								}
							}
						});				
			}
		}
	}
	
	@Command
	@NotifyChange({"modal","dtoPo","dtoDetail","lock","dtoItem","listItem"})
	public void addDtl() {
		if (!checkPo()) {
			Messagebox.show("Please complete purchase order data");
		} else {
			TPoDto findOne = poSvc.findOne(dtoPo.getPoNo());
			if (findOne != null && !flagEdit) {
				Messagebox.show("PO Number ID Already Exist");
			} else {
				countAging();
				if (aging<1) {
					Messagebox.show("Aging cant below 1");
				} else if (dtoPo.getPoShipment()== null) {
					Messagebox.show("Please select shipment");
				} else {
					dtoDetail = new TPoDetailDto();
					listItem = itemSvc.findAllItemBySupId(dtoSupp.getSupId());
					dtoItem = null;
					setModal(true);
					setLock(true);
				}				
			}
		}
	}
	
	@Command
	@NotifyChange({"modal","dtoDetail"})
	public void cancelDtl() {
		setModal(false);
		dtoDetail = new TPoDetailDto();
	}
	
	@Command
	@NotifyChange({"modal","dtoPo","dtoDetail","listDetail"})
	public void saveDtl() {
		dtoDetail.setItemId(dtoItem.getItemId());
		dtoDetail.setPoNo(dtoPo.getPoNo());
		if (checkDetail()) {
			//melakukan update listDetail
			sumTotal();
			dtoDetail = new TPoDetailDto();
			setModal(false);
		}else {
			dtoDetail.setItemPrice(dtoItem.getItemPrice());
			sumSubTotal();
			listDetail.add(dtoDetail);
			sumTotal();
			dtoDetail = new TPoDetailDto();
			setModal(false);
		}
	}
	
	@Command
	@NotifyChange({"dtoDetail","dtoPo","listDetail"})
	public void deleteDtl() {
		if (selectedListDetail.size()<1) {
			Messagebox.show("Please select data");
		} else {
			listDetail.removeAll(selectedListDetail);
		}
		dtoDetail = new TPoDetailDto();
		sumTotal();
	}
	
	@Command
	@NotifyChange({"dtoCity","listCity"})
	public void listCityByProv() {
		dtoCity = new MCityDto();
		List<MCityDto> list = citySvc.findAllCityByProvId(dtoProv.getProvId());
		if (list.size() > 0 ) {
			listCity = list;
		} else {
			Messagebox.show("No city found in "+dtoProv.getProvName());
		}
	}
	
	@Command
	@NotifyChange({"dtoDetail"})
	public void sumSubTotal() {
		dtoDetail.setItemPrice(dtoItem.getItemPrice());
		dtoDetail.setSubtotal(dtoDetail.getItemPrice()*dtoDetail.getItemQty());
	}
	
	@Command
	@NotifyChange({"dtoPo"})
	public void sumTotal() {
		int total = 0;
		int diskon = dtoPo.getDiscount();
		for (TPoDetailDto detail : listDetail) {
			total = total + detail.getSubtotal();
		}
		
		dtoPo.setTotal((total*(100-diskon))/100);
	}
	
	@Command
	@NotifyChange({"aging","dtoPo"})
	public void countAging() {
		if (dtoPo.getPoDate() != null && dtoPo.getPoExpDate()!= null) {
			aging = (dtoPo.getPoExpDate().getTime() - dtoPo.getPoDate().getTime())/86400000;
			if (aging < 1) {
				dtoPo.setPoDate(null);
				dtoPo.setPoExpDate(null);
				aging = 0;
				Messagebox.show("Aging cant below 1");
			}
		}
	}
	
	public boolean checkPo(){
		if (dtoPo.getPoNo() == null || dtoSupp.getSupId() == null || dtoCity.getCityId() == null) {
			return false;
		} else if (dtoPo.getPoNo().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean checkDetail() {
		for (TPoDetailDto det : listDetail) {
			if (det.getPoNo().equalsIgnoreCase(dtoDetail.getPoNo()) && det.getItemId().equalsIgnoreCase(dtoDetail.getItemId())) {
				dtoDetail.setItemQty((dtoDetail.getItemQty()+det.getItemQty()));
				sumSubTotal();
				listDetail.remove(det);
				listDetail.add(dtoDetail);
				return true;
			}
		}
		return false;
	}
	
	public TPoDto getDtoPo() {
		return dtoPo;
	}
	public void setDtoPo(TPoDto dtoPo) {
		this.dtoPo = dtoPo;
	}
	public List<TPoDetailDto> getListDetail() {
		return listDetail;
	}
	public void setListDetail(List<TPoDetailDto> listDetail) {
		this.listDetail = listDetail;
	}
	public List<TPoDetailDto> getSelectedListDetail() {
		return selectedListDetail;
	}
	public void setSelectedListDetail(List<TPoDetailDto> selectedListDetail) {
		this.selectedListDetail = selectedListDetail;
	}
	
	public TPoDetailDto getDtoDetail() {
		return dtoDetail;
	}
	public void setDtoDetail(TPoDetailDto dtoDetail) {
		this.dtoDetail = dtoDetail;
	}
	public List<MSupplierDto> getListSupp() {
		return listSupp;
	}
	public void setListSupp(List<MSupplierDto> listSupp) {
		this.listSupp = listSupp;
	}
	public MSupplierDto getDtoSupp() {
		return dtoSupp;
	}
	public void setDtoSupp(MSupplierDto dtoSupp) {
		this.dtoSupp = dtoSupp;
	}
	public List<MItemDto> getListItem() {
		return listItem;
	}
	public void setListItem(List<MItemDto> listItem) {
		this.listItem = listItem;
	}
	public MItemDto getDtoItem() {
		return dtoItem;
	}
	public void setDtoItem(MItemDto dtoItem) {
		this.dtoItem = dtoItem;
	}
	public List<MProvinceDto> getListProv() {
		return listProv;
	}
	public void setListProv(List<MProvinceDto> listProv) {
		this.listProv = listProv;
	}
	public MProvinceDto getDtoProv() {
		return dtoProv;
	}
	public void setDtoProv(MProvinceDto dtoProv) {
		this.dtoProv = dtoProv;
	}
	
	public List<MCityDto> getListCity() {
		return listCity;
	}
	public void setListCity(List<MCityDto> listCity) {
		this.listCity = listCity;
	}
	public MCityDto getDtoCity() {
		return dtoCity;
	}
	public void setDtoCity(MCityDto dtoCity) {
		this.dtoCity = dtoCity;
	}
	
	public long getAging() {
		return aging;
	}

	public void setAging(long aging) {
		this.aging = aging;
	}

	public boolean isFlagEdit() {
		return flagEdit;
	}
	public void setFlagEdit(boolean flagEdit) {
		this.flagEdit = flagEdit;
	}
	public boolean isButtonVisible() {
		return buttonVisible;
	}
	public void setButtonVisible(boolean buttonVisible) {
		this.buttonVisible = buttonVisible;
	}
	public boolean isModal() {
		return modal;
	}
	public void setModal(boolean modal) {
		this.modal = modal;
	}
	public boolean isLock() {
		return lock;
	}
	public void setLock(boolean lock) {
		this.lock = lock;
	}
	
	
	
}
