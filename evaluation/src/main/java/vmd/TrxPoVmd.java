package vmd;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Messagebox;

import dto.TPoDto;
import service.TPoSvc;

@VariableResolver(DelegatingVariableResolver.class)
public class TrxPoVmd {

	@WireVariable
	TPoSvc poSvc;

	private List<TPoDto> listPo = new ArrayList<>();
	private TPoDto dtoPo = new TPoDto();
	private String search;

	@Init
	public void load() {
		Sessions.getCurrent().removeAttribute("obj");
		dtoPo = new TPoDto();
		listPo = poSvc.findAllPoBySearch("");
	}

	@Command
	@NotifyChange("listPo")
	public void cekall() {
		System.err.println("masuk");
		if (listPo.size() != 0) {
			for (TPoDto t : listPo) {
				t.setCheked(true);
				listPo.add(t);
			}
		}

	}

	@Command
	public void add() {
		dtoPo = new TPoDto();
		Executions.sendRedirect("TrxPoDetail.zul");
	}

	@Command
	public void edit() {
		if (dtoPo.getPoNo() != null) {
			Sessions.getCurrent().setAttribute("obj", dtoPo);
			Executions.sendRedirect("TrxPoDetail.zul");
		} else {
			Messagebox.show("Please select data");
		}
	}

	@Command
	@Listen("onClick")
	@NotifyChange("listPo")
	public void delete() {
		if (dtoPo.getPoNo() != null) {
			Messagebox.show(
					"Are you sure want to delete Po Number : "
							+ dtoPo.getPoNo() + "?", "Delete Purchase Order?",
					Messagebox.YES | Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						@Override
						public void onEvent(Event event) throws Exception {
							// TODO Auto-generated method stub
							if (Messagebox.ON_YES.equals(event.getName())) {
								poSvc.delete(dtoPo.getPoNo());
								listPo.remove(dtoPo);
								Executions.sendRedirect("TrxPo.zul");
							} else {
								Messagebox.show("Delete Po Number : "
										+ dtoPo.getPoNo() + " Canceled");
							}
						}
					});
		} else {
			Messagebox.show("Please select data");
		}
	}

	@Command
	@NotifyChange("listPo")
	public void search() {
		List<TPoDto> listSearch = poSvc.findAllPoBySearch(search);
		if (listSearch.size() > 0) {
			listPo = listSearch;
		} else {
			listPo = poSvc.findAllPoBySearch("");
			Messagebox.show("Data not found");
		}
		dtoPo = new TPoDto();
	}

	public List<TPoDto> getListPo() {
		return listPo;
	}

	public void setListPo(List<TPoDto> listPo) {
		this.listPo = listPo;
	}

	public TPoDto getDtoPo() {
		return dtoPo;
	}

	public void setDtoPo(TPoDto dtoPo) {
		this.dtoPo = dtoPo;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}



}
